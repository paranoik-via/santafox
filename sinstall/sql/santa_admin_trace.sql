DROP TABLE IF EXISTS `%PREFIX%_admin_trace`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_admin_trace` (
  `id_admin` int(8) unsigned NOT NULL,
  `time` datetime NOT NULL,
  `place` varchar(255) NOT NULL,
  `ip` varchar(255) NOT NULL,
  `host` varchar(255) NOT NULL,
  PRIMARY KEY  (`id_admin`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
-- sqlseparator------------------------------------------------
INSERT INTO `%PREFIX%_admin_trace` VALUES ('1','0000-00-00 00:00:00','structure','127.0.0.1','server');