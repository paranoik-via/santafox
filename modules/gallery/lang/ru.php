<?php
$type_langauge							= 'ru';
$il['gallery_modul_base_name']          = 'Галерея';
$il['gallery_modul_base_name1']          = 'Галерея';
$il['gallery_property_img_big_width']  = 'Ширина большого изображения';
$il['gallery_property_img_big_height']  = 'Высота большого изображения';
$il['gallery_property_img_small_width']  = 'Ширина маленького изображения';
$il['gallery_property_img_small_height']  = 'Высота маленького изображения';
$il['gallery_list_delete_element_alert']    = 'Изображение будет удалёно. Продолжить?';
$il['gallery_pub_create_content']       = 'Вывести содержимое галереи';
$il['gallery_module_pub1_propertes3']  = 'Шаблон галереи';

$il['gallery_property_path_to_copyright_file'] = 'Файл водяного знака';
$il['gallery_property_copyright_transparency'] = 'Прозрачность водяного знака';

$il['gallery_property_add_copyright'] = 'Добавить водяной знак?';
$il['gallery_property_copyright_position'] = 'Расположение водяного знака';
$il['gallery_property_copyright_position_0'] = 'По центру';
$il['gallery_property_copyright_position_1'] = 'Сверху-слева';
$il['gallery_property_copyright_position_2'] = 'Сверху-справа';
$il['gallery_property_copyright_position_3'] = 'Снизу-справа';
$il['gallery_property_copyright_position_4'] = 'Снизу-слева';


$il['gallery_pub_random_photos'] = 'Случайные изображения';
$il['gallery_items_per_page'] = 'Элементов на страницу';
$il['gallery_items_to_show'] = 'Кол-во выводимых изображений';
$il['gallery_pub_show_photos_sort'] = 'Отобразить изображения с учетом сортировки';

$il['gallery_module_label_block_menu'] = 'Основные действия';
$il['gallery_menu_photos'] = 'Изображения';
$il['gallery_menu_add'] = 'Добавить изображение';
$il['gallery_menu_cats'] = 'Категории';

$il['gallery_save'] = 'Сохранить';
$il['gallery_edit'] = 'Редактировать';
$il['gallery_delete'] = 'Удалить';

$il['gallery_add_category'] = 'Добавить категорию';
$il['gallery_add_delete_category'] = 'Добавить/редактировать категорию';
$il['gallery_list_category'] = 'Cписок категорий';

$il['gallery_delete_category'] = 'Удалить';
$il['gallery_category_name'] = 'Название';
$il['gallery_category_delete_alert'] = 'Действительно удалить категорию (изображения из неё НЕ будут удалены)';

$il['gallery_imageform_header'] = 'Редактирование изображения';
$il['gallery_imageform_category'] = 'Категория';
$il['gallery_imageform_category_empty'] = '-не выбрано (без категории)-';
$il['gallery_list_all_cats'] = '-все категории-';

$il['gallery_import_archive'] = 'Импорт архива';
$il['gallery_import_archive_title'] = 'Импорт архива с изображениями';
$il['gallery_import_upload_file'] = 'Загрузить архив';
$il['gallery_import_file_on_server'] = 'Из архива на сервере';
$il['gallery_import_from'] = 'Импорт из';
$il['gallery_import_gen_names'] = 'Названия изображений';
$il['gallery_import_gen_names_filename'] = 'Имя файла изображения';
$il['gallery_import_gen_names_number'] = 'Порядковые номера (1..N)';
$il['gallery_import_upload_failed'] = 'Ошибка при загрузке файла';
$il['gallery_import_archive_read_failed'] = 'Ошибка чтения архива';
$il['gallery_import_archive_put_failed'] = 'Ошибка сохранения файла изображения';
$il['gallery_import_added'] = 'Добавлено изображений: ';
$il['gallery_category_descr'] = 'Описание категории';

$il['gallery_items_sorting'] ='Сортировка по:';
$il['gallery_order_num'] ='Порядок сортировки';
$il['gallery_order_date'] ='Дата';
$il['gallery_order_ID'] ='ID';
$il['gallery_describe'] ='Описание';
$il['gallery_operation'] ='Управление';
$il['gallery_property_sort_ask'] ='ID по возрастанию';
$il['gallery_property_sort_desk'] ='ID по убыванию';
$il['gallery_property_sort_order_num_ask'] ='По полю Порядок сортировки по возрастанию';
$il['gallery_property_sort_order_num_desk'] ='По полю Порядок сортировки по убыванию';
$il['gallery_list_photos'] ='Список изображений';
$il['gallery_file'] = 'файл';
$il['gallery_image_name'] = 'Название изображения';
$il['gallery_update_image'] = 'Обновить изображение';
$il['gallery_image'] = 'Изображение';